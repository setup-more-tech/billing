FROM ubuntu:latest
WORKDIR /app
COPY . /app
RUN apt-get update &&\
    apt-get -y install python3 &&\
    apt-get -y install python3-pip &&\
    pip3 install --upgrade pip &&\
    pip3 --no-cache-dir install -r requirements.txt &&\
    apt-get update
EXPOSE 5000
ENTRYPOINT ["python3","app.py"]